# Architecture Kata - Grupo 2
Fecha: 2022-05-28
## Integrantes

- Daniel Burbano
- Paul Velasco
- Nataly Hernández
- Víctor Hugo Cofre

## KATA: All Stuff, No Cruft 🚀

El organizador de la conferencia necesita un sistema de gestión para las conferencias que organiza, tanto para los ponentes como para los asistentes.

Usuarios: cientos de ponentes, decenas de personal de eventos, miles de asistentes
Requisitos:
- los asistentes pueden acceder al programa de conferencias en línea, incluidas las asignaciones de salas
- los oradores pueden administrar las charlas (ingresar, editar, modificar)
- charlas de 'votar a favor/en contra' de los asistentes
- el organizador puede notificar a los asistentes sobre los cambios de horario al minuto (si los asistentes optan por participar)
- cada conferencia (al ser un tema diferente) se puede marcar de forma independiente
- las diapositivas de los oradores están disponibles en línea solo para los asistentes
- sistema de evaluación a través de página web, correo electrónico, SMS o teléfono

Contexto adicional:

- La conferencia se lleva a cabo en los EE. UU.
- Personal de apoyo muy reducido.
- Tráfico 'ráfaga': extremadamente ocupado cuando se está produciendo una conferencia.
- El organizador de la conferencia quiere 'revestir' fácilmente el sitio para diferentes ofertas tecnológicas.

## Requerimientos funcionales 📗
**Usuarios**

	Roles:
	1. Ponente(Orador)
		  - Acceso, administración de la charla (Ingresar, editar, modificar).
		  - Gestión 
			  - Recursos (Diapositivas)
			  - Administración de charla
	2. Organizador(Personal Eventos)
		- Notificación a los asistentes registrados en las charlas.
		- Gestión:
			- Administración de charlas
			- Manejo de horario (Cambio de horario).
	3. Asistente
		- Acceso:
		- Al programa de conferencias en línea
		- A las salas creadas
		- Votar a favor/ en contra de la charla

**Sistemas de evaluación**

	- Página web
	- Correo Electrónico
	- SMS o teléfono
	- Evaluación Integrada a cada charla.
	- (Apunta a la base de datos de cada chala especifica)
	
**Operaciones**

	- Usuarios
	- Recursos (Diapositivas)
	- Administración de charlas
	- Asignación de salas
	- Manejo de horario

**Administración**

	- CRUD Sala de Conferencias
	- CRUD Charlas
	- CRUD Diapositivas
	- Manejar Notificaciones
	- Enviar Evaluaciones

## Requerimientos no funcionales 📒
- Base de datos
- SLA 99%
- Escalabilidad horizontal del componente al 80% de los recursos
- Ancho de banda suficiente para un flujo grande de usuarios.
- Multiplataforma (Aplicación Web/ Móvil)
- Buen rendimiento (Performance)
- Amigable para el usuario
- Hosting U.S
- Llamativo a la vista
- Seguridad
- Eficiencia
- Documentación y capacitación

## Módulos 📝

- Usuarios
- Recursos
- Evaluaciones
- Salas
- Charlas

## ADRs 📚

- [adr-1-cliente-web](ADR/adr-1-cliente-web.md)
- [adr-2-framework-react](ADR/adr-2-framework-react.md)
- [adr-3-api-gateway](ADR/adr-3-api-gateway.md)
- [adr-4-microservicios](ADR/adr-4-microservicios.md)
- [adr-5-base-datos](ADR/adr-5-base-datos.md)
- [adr-6-Elastix](ADR/adr-6-Elastix.md)
- [adr-6-SLA-Dinatrace](ADR/adr-7-SLA-Dinatrace.md)

# Diagramas 📍

El modelo C4 es un enfoque de "abstracción primero" para diagramar la arquitectura del software, basado en abstracciones que reflejan cómo los arquitectos y desarrolladores de software piensan y construyen el software. 
> **Nota:** Por motivos practicos unicamente se diagramo los 3 primeros niveles.

**Diagrama de contexto**

![Diagrama de contexto](Diagrams/structurizr-SistemadeConferenciaC1.png)
![Diagrama de contexto](Diagrams/structurizr-SistemadeConferenciaC1-key.png)

**Diagrama de contenedores**

![Diagrama de contenedores](Diagrams/structurizr-SistemadeConferenciaC2.png)
![Diagrama de contenedores](Diagrams/structurizr-SistemadeConferenciaC2-key.png)

**Diagrama de componentes**

![Diagrama de componentes](Diagrams/structurizr-SistemadeConferenciaC3.png)
![Diagrama de componentes](Diagrams/structurizr-SistemadeConferenciaC3-key.png)

# Código structurizr 💻
El código resultante se pude acceder desde [aquí](Diagrams/dsl)
 

    workspace "C4 Model Group 2" "Sistema de gestión de conferencias." {
     model {
      
     assistantUser = person "Assistant" "Usuario que puede acceder al programa de conferencias en línea"
     speakerUser = person "Speaker" "Gestionar charlas"
     staffUser = person "Staff" "Notifica a los asistentes sobre cambios de horario"
      
     enterprise "Conference System" {
     softwareSystem = softwareSystem "Sistema de conferencias" "Pueden visualizar las diapositivas de un orador. Pueden acceder a las salas " {
     webapp = container "Aplicación Web" "Template HTML responsivo para Web y Móvil." "Responsive HTML Template - REACT " 
     webRender = container "React Web" "Proporciona toda la funcionalidad del Sistema de Conferencias por Internet a los Usuarios por medio de un Navegador de Internet" "React" {
     tags Webrender
     }
     mobileRender = container "React Movil" "Proporciona un conjunto de funciones para todos los usuarios dependiendo su Rol, disponible para S.O Android y iOS" "React Native" {
     tags Mobilerender
     }
     apiGateway = container "API Gateway" "Conecta con toda las funcionalidades del sistema de gestión de conferencias." "Software System"{
      
     apiComponent = component "API Gateway" "Los usuarios pueden inicar sesón en el Sistema de Conferencias" "REST API"
     securityComp = component "Componente de Seguridad" "Funcionalidad de inicio de sesión único con Tokens y cambio de contraseñas" "Single Sign On, JWT"
     mailComp = component "Componente de E-mail" "Envío de evaluaciones por E-mail Envío de Recuperación de contraseña" "PHP DAO, DTO" 
     systemMainframe = component "Mainframe Sistema de Conferencias" "Asignacion de Salas para cada Conferencia" "PHP DAO, DTO"
     loginController = component "Login Controlador" "Los usuarios pueden inicar sesón en el Sistema de Conferencias" "MVC, Laravel REST Controlador"
     backUpPasswordController = component "Recuperar Contraseña Controlador" "Los usuarios pueden recuperar su contraseña en el Sistema" "MVC, Laravel REST Controlador"
     evaluationController = component "Evaluaciones Controlador" "-Elastix -Gestión de Evaluaciones" "MVC, Laravel REST Controlador"
     conferenceController = component "Conferencias Controlador" "Gestión de Conferencias" "MVC, Laravel REST Controlador"
      
     }
     dataBase = container "BASE DE DATOS" "Almacenamiento de usuarios, roles, salas, evaluaciones, charlas." "PostgreSQL 14" {
     tags Database
     dataBaseComp = component "BASE DE DATOS" "Almacenamiento de usuarios, roles, salas, evaluaciones, charlas." "PostgreSQL 14" {
     tags Database
     }
     }
     }
     }
     # relationships to/from systemContext
     assistantUser -> softwareSystem "Asistir a la conferencia, votar a favor/en contra"
     speakerUser -> softwareSystem "Administra las salas de conferencias (ingresar,editar)"
     staffUser -> softwareSystem "Notifica a los asistentes sobre cambios de horario"
      
     # relationships to/from containers
     assistantUser -> webapp "Asistir a la conferencia, votar a favor/en contra"
     speakerUser -> webapp  "Administra las salas de conferencias (ingresar,editar)"
     staffUser -> webapp  "Notifica a los asistentes sobre cambios de horario"
     webapp -> webRender "React App"
     webapp -> mobileRender "React App"
     webRender -> apiGateway "Llamadas API por: [JSON/HTTPS]"
     mobileRender -> apiGateway "Llamadas API por: [JSON/HTTPS]"
     apiGateway -> dataBase "Lee y escribe a [JDBC]" 
      
     # relationships to/from components
     webRender -> apiComponent "Llamadas API por: [JSON/HTTPS]"
     mobileRender -> apiComponent "Llamadas API por: [JSON/HTTPS]"
     apiComponent -> loginController "Llamadas API por: [JSON/HTTPS]"
     apiComponent -> backUpPasswordController "Llamadas API por: [JSON/HTTPS]"
     apiComponent -> evaluationController "Llamadas API por: [JSON/HTTPS]"
     apiComponent -> conferenceController "Llamadas API por: [JSON/HTTPS]"
     loginController -> securityComp "Uses"
     backUpPasswordController -> securityComp "Uses"
     backUpPasswordController -> mailComp "Uses"
     evaluationController -> mailComp "Uses"
     conferenceController -> systemMainframe "Uses"
     securityComp -> dataBase "Lectura y Escritura [JDBC]"
     }
     views {
     systemContext softwareSystem "SistemadeConferenciaC1" "Sistema de conferencias C1 System" {
     include *
     autoLayout 
     }
     container softwareSystem "SistemadeConferenciaC2" "Sistema de conferencias C2 Containers" {
     include *
     autoLayout
     }
     component apiGateway "SistemadeConferenciaC3" "Sistema de conferencias C3 Components" {
     include *
     autoLayout
     }
     styles {
     element "Software System" {
     background #1168bd
     color #ffffff
     }
     element "Person" {
     shape person
     background #08427b
     color #ffffff
     }
     element "Container" {
     background #1061B0
     color #ffffff
     }
     element "Database" {
     shape Cylinder
     background #1061B0
     color #ffffff
     }
     element "Component" {
     background #63BEF2
     color #ffffff
     }
     element "Webrender" {
     shape WebBrowser
     }
     element "Mobilerender" {
     shape MobileDeviceLandscape
     }
     }
     } 
    } 



