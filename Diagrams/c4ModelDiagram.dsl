workspace "C4 Model Group 2" "Sistema de gestión de conferencias." {

    model {
    
        assistantUser = person "Assistant" "Usuario que puede acceder al programa de conferencias en línea"
        speakerUser = person "Speaker" "Gestionar charlas"
        staffUser = person "Staff" "Notifica a los asistentes sobre cambios de horario"
        
        enterprise "Conference System" {
            softwareSystem = softwareSystem "Sistema de conferencias" "Pueden visualizar las diapositivas de un orador. Pueden acceder a las salas " {
                webapp = container "Aplicación Web" "Template HTML responsivo para Web y Móvil." "Responsive HTML Template - REACT " 
                webRender = container "React Web" "Proporciona toda la funcionalidad del Sistema de Conferencias por Internet a los Usuarios por medio de un Navegador de Internet" "React" {
                    tags Webrender
                }
                mobileRender = container "React Movil" "Proporciona un conjunto de funciones para todos los usuarios dependiendo su Rol, disponible para S.O Android y iOS" "React Native" {
                    tags Mobilerender
                }
                apiGateway = container "API Gateway" "Conecta con toda las funcionalidades del sistema de gestión de conferencias." "Software System"{
                    
                    apiComponent = component "API Gateway" "Los usuarios pueden inicar sesón en el Sistema de Conferencias" "REST API"
                    securityComp = component "Componente de Seguridad" "Funcionalidad de inicio de sesión único con Tokens y cambio de contraseñas" "Single Sign On, JWT"
                    mailComp = component "Componente de E-mail" "Envío de evaluaciones por E-mail Envío de Recuperación de contraseña" "PHP DAO, DTO"     
                    systemMainframe = component "Mainframe Sistema de Conferencias" "Asignacion de Salas para cada Conferencia" "PHP DAO, DTO"
                    loginController = component "Login Controlador" "Los usuarios pueden inicar sesón en el Sistema de Conferencias" "MVC, Laravel REST Controlador"
                    backUpPasswordController = component "Recuperar Contraseña Controlador" "Los usuarios pueden recuperar su contraseña en el Sistema" "MVC, Laravel REST Controlador"
                    evaluationController = component "Evaluaciones Controlador" "-Elastix -Gestión de Evaluaciones" "MVC, Laravel REST Controlador"
                    conferenceController = component "Conferencias Controlador" "Gestión de Conferencias" "MVC, Laravel REST Controlador"
                    
                }
                dataBase = container "BASE DE DATOS" "Almacenamiento de usuarios, roles, salas, evaluaciones, charlas." "PostgreSQL 14" {
                    tags Database
                    dataBaseComp = component "BASE DE DATOS" "Almacenamiento de usuarios, roles, salas, evaluaciones, charlas." "PostgreSQL 14" {
                    tags Database
                    }
                }
            }
        }

        # relationships to/from systemContext
        assistantUser -> softwareSystem "Asistir a la conferencia, votar a favor/en contra"
        speakerUser -> softwareSystem "Administra las salas de conferencias (ingresar,editar)"
        staffUser -> softwareSystem "Notifica a los asistentes sobre cambios de horario"
        
        # relationships to/from containers
        assistantUser -> webapp "Asistir a la conferencia, votar a favor/en contra"
        speakerUser -> webapp  "Administra las salas de conferencias (ingresar,editar)"
        staffUser -> webapp  "Notifica a los asistentes sobre cambios de horario"
        webapp -> webRender "React App"
        webapp -> mobileRender "React App"
        webRender -> apiGateway "Llamadas API por: [JSON/HTTPS]"
        mobileRender -> apiGateway "Llamadas API por: [JSON/HTTPS]"
        apiGateway -> dataBase "Lee y escribe a [JDBC]"        
        
        # relationships to/from components
        webRender -> apiComponent "Llamadas API por: [JSON/HTTPS]"
        mobileRender -> apiComponent "Llamadas API por: [JSON/HTTPS]"
        apiComponent -> loginController "Llamadas API por: [JSON/HTTPS]"
        apiComponent -> backUpPasswordController "Llamadas API por: [JSON/HTTPS]"
        apiComponent -> evaluationController "Llamadas API por: [JSON/HTTPS]"
        apiComponent -> conferenceController "Llamadas API por: [JSON/HTTPS]"
        loginController -> securityComp "Uses"
        backUpPasswordController -> securityComp "Uses"
        backUpPasswordController -> mailComp "Uses"
        evaluationController -> mailComp "Uses"
        conferenceController -> systemMainframe "Uses"
        securityComp -> dataBase "Lectura y Escritura [JDBC]"
    }

    views {
        systemContext softwareSystem "SistemadeConferenciaC1" "Sistema de conferencias C1 System" {
            include *
            autoLayout 
        }
        container softwareSystem "SistemadeConferenciaC2" "Sistema de conferencias C2 Containers" {
            include *
            autoLayout
        }
        component apiGateway "SistemadeConferenciaC3" "Sistema de conferencias C3 Components" {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "Container" {
                background #1061B0
                color #ffffff
            }
            element "Database" {
                shape Cylinder
                background #1061B0
                color #ffffff
            }
            element "Component" {
                background #63BEF2
                color #ffffff
            }
            element "Webrender" {
                shape WebBrowser
            }
            element "Mobilerender" {
                shape MobileDeviceLandscape
            }
        }
    }
    
}
