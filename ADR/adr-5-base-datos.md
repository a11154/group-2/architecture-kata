
# [Base de datos de cada Microservicio]

* Status: [accepted](adr-5-bdd.md) <!-- optional -->
* Deciders: [Daniel Burbano, Paul Velasco, Nataly Hernández, Víctor Hugo Cofre] <!-- optional -->
* Date: [2022-05-21] <!-- optional -->

Technical Story: [Architecture Kata] <!-- optional -->

## Context and Problem Statement
* El sistema de gestión de conferencias tiene un requerimiento de un programa en linea. 
* Este sistema debe soportar su uso en escritorio web y movil.

## Decision Drivers <!-- optional -->

* Escoger una alternativa de almacenamiento escalable, facil de mantener y que se adapte al diseño web.
<!-- numbers of drivers can vary -->

## Considered Options

* Una base de datos individual para cada microservicio
* Una base de datos centralizada
<!-- numbers of options can vary -->

## Decision Outcome
"[Una base de datos individual para cada microservicio]", porque nos permite a futuro aumentar la capacidad conforme crecimiento, facilitando un mantenimiento sencillo.

### Positive Consequences <!-- optional -->

* Escalable.
* Disponibilidad.
* Facil mantenimiento.


## Pros and Cons of the Options <!-- optional -->

### [Una base de datos individual para cada microservicio]

* Bueno, su estructura es sencilla y el mantenimiento seria más facil.
* Malo, para respaldar el sistema se debe considerar cada bdd.

### [Una base de datos centralizada]

* Bueno, el respaldo se centraliza en un sola bdd.
* Malo, la escalabilidad y recursos se centran en una sola bdd.
