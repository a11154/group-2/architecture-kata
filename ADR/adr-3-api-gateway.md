
# [API GATEWAY]

  

* Status: [accepted](adr-1-cliente-web.md) <!-- optional -->

* Deciders: [Daniel Burbano, Paul Velasco, Nataly Hernández, Víctor Hugo Cofre] <!-- optional -->

* Date: [2022-05-21] <!-- optional -->

  

Technical Story: [Architecture Kata] <!-- optional -->

  

## Context and Problem Statement

* El sistema de gestión de conferencias al ser una Arquitectura basada en Microservicios necesita un componente que se posicione cara a los microservicios, controlando el acceso y la funcionalidad.

* Exponer únicamente los recursos necesarios a la red pública y ocultamos el resto de información.

  

## Decision Drivers <!-- optional -->

  

* Uso de una API de comunicación.
* Implementar un sistema de Inicio de Sesión único (Single Sign On – SSO).

<!-- numbers of drivers can vary -->

  

## Considered Options

  

* REST API para la comunicación

* Seguridad Basada en Tokens para el SSO

<!-- numbers of options can vary -->

  

## Decision Outcome

**"[REST API]", por su escalabilidad, su estandarización y su separación entre cliente y servidor**

  

### Positive Consequences <!-- optional -->

  

* REST requiere menos recursos del servidor
* Una característica de REST es que no guarda estado; es indiferente qué servidor atienda cada solicitud, lo cual es óptimo para el Sistema de Conferencias.
* Independencia de tecnologías / lenguajes

### Negative Consequences <!-- optional -->
  
* Requiere un mayor esfuerzo, debido a que es conveniente la realización de pruebas

**"[Seguridad Basada en Tokens ]", autenticar a los usuarios y emitir tokens**
 

### Positive Consequences <!-- optional -->


* Más seguridad y confiabilidad para las usuarios del Sistema de Conferencias.

* La validación del Token puede detectar inactividad en el Sistema de Conferencias

### Negative Consequences <!-- optional -->

  

* Una cookie de sesión es relativamente pequeña en comparación (incluso) con el token

   
## Pros and Cons of the Options <!-- optional -->

  
### [REST API]

  

* Bueno, para la comunicación Multiplataforma

* Malo, mas tiempo de desarollo en creación de API's


### [Seguridad Basada en Tokens]


* Bueno, proporciona inicio de sesión único.

* Malo, si el token caduca, se genera un error.