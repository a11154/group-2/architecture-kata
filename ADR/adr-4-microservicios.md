# [MICROSERVICIOS]

* Status: [accepted](adr-4-microservicios.md) <!-- optional -->
* Deciders: [Daniel Burbano, Paul Velasco, Nataly Hernández, Víctor Hugo Cofre] <!-- optional -->
* Date: [2022-05-21] <!-- optional -->

Technical Story: [Architecture Kata] <!-- optional -->

## Context and Problem Statement
* El sistema de gestión de conferencias requiere acceder desde varias plataformas.
* Necesidad de escalar el sistema a futuro.

## Decision Drivers <!-- optional -->

* Uso de microservicios
<!-- numbers of drivers can vary -->

## Considered Options

* Monolítico
* Microservicios

<!-- numbers of options can vary -->

## Decision Outcome
"[Microservicios]", porque nos permite acceder desde varias tecnologías y escalar facilmente.

### Positive Consequences <!-- optional -->

* Escalable.
* Multiplataforma ( IOS / Andriod / Escritorio / Web)
* Fácil mantenimiento.
* Balanceo de carga acorde a necesidades.
* Seguridad
* Más rápida 

### Negative Consequences <!-- optional -->

* Conocimiento previo.


## Pros and Cons of the Options <!-- optional -->

### [Monolítico]

* Bueno, porque no necesitas conocimiento avanzado de arquitectura.
<!-- numbers of pros and cons can vary -->
* Malo, porque no es facil la escalabilidad.


### [Microservicios]

* Bueno, porque es escalable.
* Bueno, porque permite multiplataforma ( IOS / Andriod / Escritorio / Web)
* Bueno, porque fácil mantenimiento.
* Bueno, porque permite balancear las cargas acorde a necesidades.
* Seguridad
* Más rápida 

* Malo, porque se requiere conocimientos técnicos avanzados