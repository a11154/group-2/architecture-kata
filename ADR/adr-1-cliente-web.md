# [CLIENTE WEB]

* Status: [accepted](adr-1-cliente-web.md) <!-- optional -->
* Deciders: [Daniel Burbano, Paul Velasco, Nataly Hernández, Víctor Hugo Cofre] <!-- optional -->
* Date: [2022-05-21] <!-- optional -->

Technical Story: [Architecture Kata] <!-- optional -->

## Context and Problem Statement
* El sistema de gestión de conferencias tiene un requerimiento de un programa en linea. 
* Este sistema debe soportar su uso en escritorio web y movil.

## Decision Drivers <!-- optional -->

* Escoger un cliente web responsivo para adaptabilidad en web y móvil.
<!-- numbers of drivers can vary -->

## Considered Options

* Escritorio
* Web
* Móvil
<!-- numbers of options can vary -->

## Decision Outcome
"[Web]", porque nos permite a futuro aumentar la oferta tecnológica.

### Positive Consequences <!-- optional -->

* Llegar a más usuarios, fácil acceso desde cualquier dispositivo.
* Mejorar oferta tecnológica.
### Negative Consequences <!-- optional -->

* Compatibilidad con navegadores deprecados.

## Pros and Cons of the Options <!-- optional -->

### [Escritorio]

* Bueno, porque no necesitaria de un navegador.
<!-- numbers of pros and cons can vary -->
* Malo, porque necesitaria requisitos mínimos de instalación.

### [Web]

* Bueno, acceder desde cualquier navegador.
* Malo, versión del navegador actual.

### [Móvil]

* Bueno, porque en la actualidad toda persona cuenta con un dispositivo móvil.
* Malo, contar con un celular actualizado, con requisitos mínimos.
