# [Framework REACT]

* Status: [accepted](adr-2-framework-react.md) <!-- optional -->
* Deciders: [Daniel Burbano, Paul Velasco, Nataly Hernández, Víctor Hugo Cofre] <!-- optional -->
* Date: [2022-05-21] <!-- optional -->

Technical Story: [Architecture Kata] <!-- optional -->

## Context and Problem Statement
* El sistema de gestión de conferencias tiene un requerimiento la futura escalabilidad a nuevas ofertas tecnológicas.
* Tener un rendimiento estable del sistema.
* Tener un codigo optimizable y que funcione en diferentes tecnologías.

## Decision Drivers <!-- optional -->

* Escoger el Framework REACT para el desarrollo.
<!-- numbers of drivers can vary -->

## Considered Options

* REACT
* VUE

<!-- numbers of options can vary -->

## Decision Outcome
"[REACT]", porque nos permite a futuro escalar a tecnologías de escritorio o móviles.

### Positive Consequences <!-- optional -->

* Escalable a nuevas ofertas tecnológicas.
* Multiplataforma ( IOS / Andriod / Escritorio / Web)
* Buen rendimiento a nivel cliente

### Negative Consequences <!-- optional -->

* Ausencia de documentación oficial.
* No existe un estándar de desarrollo, de modo que tenemos demasiadas elecciones a tomar.

## Pros and Cons of the Options <!-- optional -->

### [REACT]

* Bueno, porque es un Desarrollo rentable, React Native ofrece a los desarrolladores una vía económica para crear aplicaciones multiplataforma con React Native.
* Bueno, porque Aprovecha JavaScript
* Bueno, porque tiene un excelente rendimiento de la aplicación
<!-- numbers of pros and cons can vary -->
* Malo, porque tiene desafíos de compatibilidad y depuración.
* Malo, porque depende de Facebook, si Facebook deja de proporcionar apoyo a la plataforma se derrumbaría.


### [VUE]

* Bueno, porque tiene un tamaño pequeño y puede ser instalado en equipos con baja memoria.
* Bueno, porque puede desglosar los componentes en archivos individuales.
* Bueno, porque es fácil de aprender y utilizar

* Malo, porque la documentación está en un idioma (Mandarin) que pocos manejan.
* Malo, porque hay muy pocos expertos en VueJS en el campo.