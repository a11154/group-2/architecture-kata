
# [Elastix]

* Status: [accepted](adr-1-elastix.md) <!-- optional -->
* Deciders: [Daniel Burbano, Paul Velasco, Nataly Hernández, Víctor Hugo Cofre] <!-- optional -->
* Date: [2022-05-21] <!-- optional -->

Technical Story: [Architecture Kata] <!-- optional -->

## Context and Problem Statement
* El sistema de gestión de conferencias requiere de evaluación a través de página web, correo electrónico, SMS o teléfono

## Decision Drivers <!-- optional -->

* Escoger una alternativa de qye permita la evalaucion con los requerimientos definidos.
<!-- numbers of drivers can vary -->

## Considered Options

* Elastix
* Asterisk
<!-- numbers of options can vary -->

## Decision Outcome
"[Elastix]", porque cuenta con GUI que permite que un usuario básico pueda usarlo y configurarlo.

### Positive Consequences <!-- optional -->

* Facil administración.
* Cuenta con varios plugins para distintas necesidades.

## Pros and Cons of the Options <!-- optional -->

### [Elastix]

* Bueno, su administración permite el uso de usuarios con conocimientos básicos.
* Malo, tiene complementos instaldos que no siempre son necesarios.

### [Asterisk]

* Bueno, tiene buen soporte y apoyo.
* Malo, requiere conocimientos técnicos avanzados.
