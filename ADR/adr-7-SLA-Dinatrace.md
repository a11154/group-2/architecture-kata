# SLA Dynatrace

  

* Status: [accepted](adr-1-cliente-web.md) <!-- optional -->

* Deciders: [Daniel Burbano, Paul Velasco, Nataly Hernández, Víctor Hugo Cofre] <!-- optional -->

* Date: [2022-05-21] <!-- optional -->

  

Technical Story: [Architecture Kata] <!-- optional -->

  

## Context and Problem Statement

- Administración de los micro servicios
- Alto tráfico (Ráfaga)
- Disponibilidad del servicio
- Hospedaje cerca de Estados Unidos

  

## Decision Drivers <!-- optional -->

* Dynatrace como SLA

<!-- numbers of drivers can vary -->

## Considered Options

 AMBIT BST
 Dynatrace
<!-- numbers of options can vary -->

 
## Decision Outcome
"[Dynatrace]",  ubicacion en Estados Unidos

### Positive Consequences <!-- optional -->

- Menor latencia en la conexión.
- Tener disponibles los microservicios
- Administración de los micro servicios

### Negative Consequences <!-- optional -->

- dependencia del proveedor 



  
